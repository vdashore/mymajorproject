const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var feedbackSchema = new Schema({
    rating: {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    comment: {
        type: String
    },
    person: {
        type: mongoose.Schema.Types.ObjectId
    }
});

var Feedbacks = mongoose.model('Feedback', feedbackSchema);
module.exports = Feedbacks;