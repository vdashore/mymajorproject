const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var bookServiceSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    mobileNo: {
        type: Number,
        required: true,
    },
    serviceType: {
        type: String,
        required: true,
    },
    houseNo: {
        type: String,
    },
    colony: {
        type: String,
    },
    area: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true
    },
    landmark: {
        type: String
    },
    date: {
        type: String,
        required: true
    },
    timeSlot: {
        type: String,
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        default: null
    },
    providerId: {
        type: mongoose.Schema.Types.ObjectId,
        default: null
    },
    providerName: {
        type: String,
        default: ''
    },
    providerNo: {
       type: Number,
       default: 0 
    },
    active: {
        type: Boolean,
        default: true
    },
    cancel: {
        type: mongoose.Schema.Types.ObjectId,
        default: null
    }
}, {
    timestamps: true
});

var Services = mongoose.model('Service', bookServiceSchema);

module.exports = Services;