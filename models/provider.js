const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const providerSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        unique: true
    },
    mobileNo: {
        type: Number,
        required: true,
        unique: true
    },
    serviceType: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    accountNo: {
        type: Number,
        default: 00
    },
    ifscCode: {
        type: String,
        default: ''
    },
    city: {
        type: String,
        required: true
    },
    aadharNo: {
        type: Number,
        required: true,
        default: 0
    },
    area:{
        type: [String],
        required: true
    },
    user: {
        type: Boolean,
        required: true,
        default: false
    }
});

var Providers = mongoose.model('Provider', providerSchema);

module.exports = Providers;