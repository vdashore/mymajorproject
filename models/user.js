const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    mobileNo: {
        type: Number,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    user: {
        type: Boolean,
        required: true,
        default: true
    }
});

var Users = mongoose.model('User', userSchema);

module.exports = Users;