const express = require('express');
const mongoose = require('mongoose');
const fast2sms = require('fast-two-sms');

const Services = require('../models/bookService');
const Providers = require('../models/provider');
const Users = require('../models/user');

const providerMain = express.Router().use(express.json());

providerMain.route('/:providerId') 

// To show notifications
    .get((req,res,next) => {
        Providers.findById(req.params.providerId)
            .then((pro) => {
                if(pro.city == 'Dewas')
                {
                    Services.find({"city": pro.city, "serviceType": pro.serviceType, "providerId": null, "active": true})
                        .then((services) => {
                            res.json(services);
                        }, (err) => next(err))
                        .catch((err) => next(err));                   
                }
                else 
                {
                    Services.find({"city": pro.city, "serviceType": pro.serviceType, "area": pro.area, "providerId": null, "active": true})
                        .then((services) => {
                            res.json(services);
                        }, (err) => next(err));                    
                }
                
            }, (err) => next(err)) 
            .catch((err) => next(err));
    })

    // To accept a booking , just pass serviceId in body
    .post((req, res, next) =>{
        Services.findById(req.body.serviceId)
            .then((service) => {
                if(service.providerId == null) 
                {
                    Providers.findById(req.params.providerId)
                        .then((pro) => {
                            service.providerId = pro._id;
                            service.providerName = pro.name;
                            service.providerNo = pro.mobileNo;
                            service.save()
                                .then((serv1) => {
                                res.json(serv1);
                                // res.redirect('/provider/'+req.params.providerId+'/status/');
                            }, (err) => next(err))
                            .catch((err) => next(err));
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
                else 
                {
                    res.json("Service already taken");
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    });

providerMain.route('/:providerId/profile')

// Getting Profile details
    .get((req,res,next) => {
        Providers.findById(req.params.providerId)
            .then((pro) => {
                res.json(pro);
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    // Updating information of provider
    .post((req,res,next) => {
        Providers.findById(req.params.providerId)
            .then((pro) => {
                if(pro.city == 'Dewas')
                {
                    pro.accountNo = req.body.accountNo;
                    pro.ifscCode = req.body.ifscCode;
                    pro.aadharNo = req.body.aadharNo;
                    pro.save()
                        .then((pro1) => {
                            res.json(pro1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
                else
                {
                    pro.accountNo = req.body.accountNo;
                    pro.ifscCode = req.body.ifscCode;
                    pro.aadharNo = req.body.aadharNo;
                    pro.area = req.body.area;
                    pro.save()
                        .then((pro1) => {
                            res.json(pro1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    });

providerMain.post('/:providerId/feedback', (req,res,next) => {
    Feedback.create(req.body)
        .then((feed) => {
            feed.person = req.params.providerId;
            feed.save()
                .then((feed1) => {
                    res.json('Feedback posted');
                })
                .catch((err) => next(err));
        })
        .catch((err) => next(err));
});

providerMain.route('/:providerId/previous')

// Getting details of previous services
    .get((req,res,next) => {
        Services.find({"providerId": req.params.providerId, "active": "false"})
            .then((services) => {
                res.json(services);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

providerMain.route('/:providerId/status')

// Getting status of current services
    .get((req,res,next) => {
        Services.find({"providerId": req.params.providerId, "active": "true"})
            .then((services) => {
                res.json(services);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

providerMain.route('/:providerId/status/:serviceId')

    // cancelling service for provider
    .post((req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                service.providerId = null;
                service.providerName = '';
                service.providerNo = 0;
                service.cancel = req.params.providerId;

                var msgOptions = {
                    authorization : process.env.F2S_API_KEY,
                    message : "39889",
                    sender_id : "FSTSMS",
                    route : "qt",
                    numbers: [service.mobileNo]
                };
                fast2sms.sendMessage(msgOptions)
                    .then((response) => {
                        console.log("response by msg"+response.message);
                    })
                    .catch((err) => console.log("error in msg"+err));


                service.save()
                    .then((serv) => {
                        res.json(serv);
                    }, (err) => next(err))
                    .catch((err) => next(err));
            }, (err) => next(err))
            .catch((err) => next(err));
    });

module.exports = providerMain;