const express = require('express');
const mongoose = require('mongoose');
const https = require('https');
const http = require('http');
const fast2sms = require('fast-two-sms');

require('dotenv').config();

const Services = require('../models/bookService');
const Users = require('../models/user');
const Providers = require('../models/provider');
const Feedback = require('../models/feedback');

const userMain = express.Router()
    .use(express.json());

userMain.route('/:userId/profile')
 
/* Removed paytm payments gateway due to frontend issues*/ 

//Fetching Details of the user 

    .get((req,res,next) => {
        Users.findById(req.params.userId)
            .then((user) => {
                res.json(user);
            },(err) => next(err))
            .catch((err) => next(err));
    });

userMain.post('/:userId/feedback', (req,res,next) => {
    Feedback.create(req.body)
        .then((feed) => {
            feed.person = req.params.userId;
            feed.save()
                .then((feed1) => {
                    res.json('Feedback posted');
                })
                .catch((err) => next(err));
        })
        .catch((err) => next(err));
});

userMain.route('/:userId/bookservice')

    .post((req,res,next) => {
        if(req.body.city == 'Dewas') {
            var numbers = [];
            Providers.find({"city":req.body.city, "serviceType": req.body.serviceType}) 
                .then((pros) => {
                    for(pro of pros)
                    {
                        numbers.push(pro.mobileNo);
                    }
                    //we can write both ["number"] and [number]
                    var msgOptions = {
                        authorization : process.env.F2S_API_KEY,
                        message : "32909",
                        sender_id : "FSTSMS",
                        route : "qt",
                        numbers: numbers
                    };
                    fast2sms.sendMessage(msgOptions)
                        .then((response) => {
                            console.log("response msg "+response.message);
                        })
                        .catch((err) => console.log("error in msg "+err));

                    numbers = [];

                },(err) => next(err))
                .catch((err) => next(err));
            
            Services.create(req.body) // check redirect ,
                .then((service) => {
                    service.userId = req.params.userId;
                    service.save()
                        .then((serv1) => {
                            res.json(serv1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }, (err) => next(err))
                .catch((err) => next(err));
        }

        else {
            var numbers = [];
            Providers.find({"city": req.body.city, "serviceType": req.body.serviceType, "area": req.body.area})
                .then((pros) => {
                    for(pro of pros)
                    {
                        numbers.push(pro.mobileNo);                        
                    }                    
                    console.log(numbers);
                    var msgOptions = {
                        authorization : process.env.F2S_API_KEY,
                        message : "32909",
                        sender_id : "FSTSMS",
                        route : "qt",
                        numbers: numbers
                    };

                    fast2sms.sendMessage(msgOptions)
                        .then((response) => {
                            console.log("response bt msg"+response.message);
                        })
                        .catch((err) => console.log("error in msg"+err));

                    numbers = [];
                }, (err)=> next(err))
                .catch((err) => next(err));

                Services.create(req.body) // check redirect ,
                .then((service) => {
                    service.userId = req.params.userId;
                    service.save()
                        .then((serv1) => {
                            res.json(serv1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }, (err) => next(err))
                .catch((err) => next(err));
        }
});

userMain.route('/:userId/previous')
    
// Getting all the previous services
    .get((req,res,next) => {
        Services.find({"userId": req.params.userId, "active": "false"})
            .then((services) => {
                res.json(services);
            }, (err) => next(err))
            .catch((err) => next(err));        
    });

userMain.route('/:userId/status')

// status of all the current active services
    .get((req,res,next) => {
        Services.find({"userId": req.params.userId, "active": "true"})
            .then((services) => {
                res.json(services);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

userMain.route('/:userId/status/:serviceId')

    //Cancelling the booking
    .post((req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                if(service.providerId == null)
                {
                    service.active = "false";
                    service.save()
                        .then((serv) => {
                            res.json("Refunds would be transferred"+serv);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
                else
                {
                    service.active = "false";
                    service.save()
                        .then((serv) => {
                            var msgOptions = {
                                authorization : process.env.F2S_API_KEY,
                                message : "33910",
                                sender_id : "FSTSMS",
                                route : "qt",
                                numbers: [service.providerNo]
                            };
                            fast2sms.sendMessage(msgOptions)
                                .then((response) => {
                                    console.log("response by msg"+response.message);
                                })
                                .catch((err) => console.log("error in msg"+err));
            

                            res.json("No refunds"+serv);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    // Updating the Time slot of service
    .put((req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                service.timeSlot = req.body.timeSlot;
                service.date = req.body.date;
                service.save()
                    .then((serv1) => {
                        res.send("TimeSlot successfully Updated");

                        var msgOptions = {
                            authorization : process.env.F2S_API_KEY,
                            message : "39887",
                            sender_id : "FSTSMS",
                            route : "qt",
                            numbers: [service.providerNo]
                        };
                        fast2sms.sendMessage(msgOptions)
                            .then((response) => {
                                console.log("response by msg"+response.message);
                            })
                            .catch((err) => console.log("error in msg"+err));
                    

                    }, (err) => next(err))
                    .catch((err) => next(err));
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    // working of done button
    .delete((req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                service.active = false;
                service.save()
                    .then((serv1) => {
                        res.json("Thank You for choosing Karigar, we're happy to help. Please provide feedback to improve us");
                    }, (err) => next(err))
                    .catch((err) => next(err));
            }, (err) => next(err))
            .catch((err) => next(err));
    })

module.exports = userMain ;