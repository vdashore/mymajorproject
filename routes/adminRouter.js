const express = require('express');
const mongoose = require('mongoose');

const Users = require('../models/user');
const Providers = require('../models/provider');
const Services = require('../models/bookService');

var loggedIn

var isValidAdmin = (req, res, next) => {
    if(loggedIn) return next()

    else {
        err = new Error('Stop messing around with admin routes');
        return next(err);
    }
}

const adminRouter = express.Router().use(express.json());

// Route to login the admin
adminRouter.post('/login', (req,res,next) => {
    if(req.body.username === "User123" && req.body.pass === "demo123")
    {
        loggedIn = true;
        res.end();
    }
    else
    {
        err = new Error('Stop messing with admin routes');
        next(err);
    }
});

// getting users related data

adminRouter.route('/user/mobileno')
    .post(isValidAdmin, (req,res,next) => {
        Users.findOne({"mobileNo": req.body.mobileNo})
            .then((user) => {
                res.json(user);
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    .put(isValidAdmin, (req,res,next) => {
        Users.findOneAndDelete({"mobileNo": req.body.mobileNo})
            .then((resp) => {
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

// Getting data related to specific provider

adminRouter.route('/provider/mobileno')
// search specific provider
    .post(isValidAdmin, (req,res,next) => {
        Providers.findOne({"mobileNo": req.body.mobileNo})
            .then((pro) => {
                res.json(pro);
            }, (err) => next(err))
            .catch((err) => nxt(err));
    })

    //Update details of a provider
    .put(isValidAdmin, (req,res,next) => {
        Providers.findOne({"mobileNo": req.body.mobileNo})
            .then((pro) => {
                if(pro.city == 'Dewas')
                {
                    pro.accountNo = req.body.accountNo;
                    pro.ifscCode = req.body.ifscCode;
                    pro.aadharNo = req.body.aadharNo;
                    pro.save()
                        .then((pro1) => {
                            res.json(pro1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
                else
                {
                    pro.accountNo = req.body.accountNo;
                    pro.ifscCode = req.body.ifscCode;
                    pro.aadharNo = req.body.aadharNo;
                    pro.area = req.body.area;
                    pro.save()
                        .then((pro1) => {
                            res.json(pro1);
                        }, (err) => next(err))
                        .catch((err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    .delete(isValidAdmin, (req,res,next) => {
        Providers.findOneAndDelete({"mobileNo": req.body.mobileNo})
            .then((resp) => {
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

//is method ko dekh k apan providers ko call karenge ya khudse alot kar denge
adminRouter.get('/service/active', isValidAdmin, (req,res,next) => {
    Services.find({"active": true})
        .then((services) => {
            res.json(services);
        }, (err) => next(err))
        .catch((err) => next(err)); 
});

//is method se refunds ki process hogi
adminRouter.get('/service/previous', isValidAdmin, (req,res,next) => {
    Services.find({"active": false})
        .then((services) => {
            res.json(services);
        }, (err) => next(err))
        .catch((err) => next(err));
});

//booking jis number se hui wo dala
adminRouter.route('/service/:serviceId')
    // Here we can assign a provider to a service by passing details in body 
    .post(isValidAdmin, (req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                service.providerId = req.body.providerId;
                service.providerName = req.body.providerName;
                service.providerNo = req.body.providerNo;
                service.save()
                    .then((serv1) => res.json(serv1))
                    .catch((err) => next(err));
            }, (err) => next(err))
            .catch((err) => next(err));
    })

    // to set active flag false for any specific service
    .put(isValidAdmin, (req,res,next) => {
        Services.findById(req.params.serviceId)
            .then((service) => {
                service.acive = false;
                service.save()
                    .then((serv1) => {
                        res.json(serv1);
                    }, (err) => next(err))
                    .catch((err) => next(err)); 
            }, (err) => next(err))
            .catch((err) => next(err));
    });

adminRouter.post('/logout', isValidAdmin, (req,res,next) => {
    loggedIn = null;
    res.json("Successfuly logged Out");
});

module.exports = adminRouter;