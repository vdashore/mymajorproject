const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
require('dotenv').config()

const Users = require('../models/user');
const Providers = require('../models/provider');

const loginRouter = express.Router()
.use(express.json());

loginRouter.route('/')
    .post((req, res, next) => {
        if(req.body.providerlogin === false)
        {
            Users.findOne({"mobileNo": req.body.mobileNo})
                .then(async (user) => {
                    if(user == null) return res.json("Not Found");
                    let flag =await bcrypt.compare(req.body.password, user.password);
                    try {
                    if(user!== null && flag == true) {
                        const accessToken = jwt.sign(user.name, process.env.ACCESS_TOKEN_SECRET);
                        res.json({accessToken : accessToken, _id: user._id, name: user.name, mobileNo: user.mobileNo, city: user.city});
                    }
                    else {
                        err = new Error("Invalid credentials");
                        res.statusCode = 403;
                        next(err);
                    }
                }
                catch(err) { console.log(err)}
                }, (err) => next(err))
                .catch((err) => next(err));
        }
        else
        {
            Providers.findOne({"mobileNo": req.body.mobileNo})
                .then(async (pro) => {
                    if(pro == null) return res.json("Not Found");
                    let flag = await bcrypt.compare(req.body.password, pro.password);
                    try {
                    if(pro!==null && flag == true) {
                        const accessToken = jwt.sign(pro.name, process.env.ACCESS_TOKEN_SECRET);
                        res.json({accessToken: accessToken, _id: pro._id, name: pro.name, mobileNo: pro.mobileNo, serviceType: pro.serviceType, city: pro.city, area: pro.area});
                    }
                    else {
                        err = new Error("Invalid credentials");
                        res.statusCode = 403;
                        next(err);
                    }
                }
                catch(err) { console.log(err) }
                }, (err) => next(err))
                .catch((err) => next(err));
        }
    });

module.exports = loginRouter;