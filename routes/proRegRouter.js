const express = require('express');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
const fast2sms = require('fast-two-sms');
require('dotenv').config();

const Providers = require('../models/provider');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.MAIL_ID,
        pass: process.env.MAIL_PASS
    }
});

const proRegRouter = express.Router()
    .use(express.json());

proRegRouter.route('/')
    .post(async (req,res,next) => {
        try {
            const hashedPassword = await bcrypt.hash(req.body.password, 10);
            req.body.password = hashedPassword;
        }
        catch(err) {
            console.log(err);
        }
        Providers.create(req.body)
            .then((pro) => {
                const mailOptions = {
                    from: process.env.MAIL_ID,
                    to: ''+req.body.email,
                    subject: 'Welcome to Karigar',
                    text: `Greetings from team Karigar
                    Welcome on board as a provider. We hope you'll enjoy our services.
                    The tech team is there to help you at any point and clear your issues.
                    You can provide your feedback at any point regarding your experience with us
                    Hope you have business growth with us.
                    
                    Regards
                    Team Karigar`
                };
                transporter.sendMail(mailOptions, (err,info) => {
                    if(err) 
                        console.log(err);
                    else
                        console.log('Mail sent '+info.response);
                });

                var msgOptions = {
                    authorization : process.env.F2S_API_KEY,
                    message : "39884",
                    sender_id : "FSTSMS",
                    route : "qt",
                    numbers: [req.body.mobileNo]
                };
                fast2sms.sendMessage(msgOptions)
                    .then((response) => {
                        console.log("response by msg"+response.message);
                    })
                    .catch((err) => console.log("error in msg"+err));


                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(pro);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

module.exports = proRegRouter;