var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dataBase = require('./models/mongoose');
var jwt = require('jsonwebtoken');
require('dotenv').config();

var indexRouter = require('./routes/index');
var userRegRouter = require('./routes/userRegRouter');
var proRegRouter = require('./routes/proRegRouter');
var loginRouter = require('./routes/login');
var userMain = require('./routes/userMain');
var providerMain = require('./routes/providerMain');
var adminRouter = require('./routes/adminRouter');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// CORS code

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS, PUT, PATCH, DELETE");
  next();
})

app.use('/', indexRouter);
app.use('/home/registeruser', userRegRouter);
app.use('/home/registerprovider', proRegRouter);
app.use('/home', loginRouter);

// add the jwt middleware here if frontend needs

app.use('/user', userMain);
app.use('/provider', providerMain);
app.use('/admin', adminRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
